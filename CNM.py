import numpy as np
import functions as fun
import maxheap as mx
from functions import is_connected

adj_mat = np.zeros((7,7))
with open('test.txt', 'rb') as f:
    for l in f:
        pair = l.replace('\n', '').split('\t')
        pair = (int(pair[0]), int(pair[1]))
        adj_mat[pair[0] - 1, pair[1] - 1] = 1
        adj_mat[pair[1] - 1, pair[0] - 1] = 1


# inisiasi
community = {i: [i] for i in xrange(adj_mat.shape[0])}
m = np.sum(adj_mat) / 2
k = np.sum(adj_mat, axis=1)

for am in adj_mat:
    print am
print m
print k

delta_Q = np.zeros(adj_mat.shape)
for sort_idx, i in np.ndenumerate(adj_mat):
    delta_Q[sort_idx] = 0
    if adj_mat[sort_idx[0]][sort_idx[1]] > 0:
        delta_Q[sort_idx] = (1 / (2 * m)) - ((k[sort_idx[0]] * k[sort_idx[1]]) / (2 * m) ** 2)
        # print sort_idx, delta_Q[sort_idx]

a = k / (2*m)
current_Q = 0
# print a
# print delta_Q
counter = 0
while len(community) > 2 :
    # max_idx = np.unravel_index(np.argmax(delta_Q), delta_Q.shape)
    # max_idx = mx.MaxHeap(np.max(delta_Q, axis=1).toList())
    max_idx = fun.unravel(delta_Q, community)
    current_Q += delta_Q[max_idx]

    # gabung i ke j (update)
    c_i = max_idx[0]
    c_j = max_idx[1]

    for c_k in community.iterkeys():
        if not (c_k == c_j or c_k == c_i) :
            connected_to_i = is_connected(adj_mat, community[c_i], community[c_k])
            connected_to_j = is_connected(adj_mat, community[c_j], community[c_k])

            if connected_to_i and connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] + delta_Q[c_j, c_k]
            elif connected_to_i:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] - (2*(a[c_j] * a[c_k]))
            elif connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_j, c_k] - (2*(a[c_i] * a[c_k]))

    # delta_Q[:, c_j] = delta_Q[c_j, :].T
    # delta_Q[c_i, :] = -np.inf
    # delta_Q[:, c_i] = -np.inf

    a[c_j] += a[c_i]
    a[c_i] = 0

    community[c_j].extend(community[c_i])
    del community[c_i]

    print max_idx,
    print current_Q
    print community
