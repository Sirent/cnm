from itertools import permutations
from functions import is_connected
import numpy as np

tag_ids = set()

with open('user_taggedbookmarks.dat') as dataset_file:
    dataset_file.readline()

    for line in dataset_file:
        line = line.split()
        tag_id = int(line[2])

        if tag_id not in tag_ids:
            tag_ids.add(tag_id)

n_selected_tag = 2000
selected_tag = set(list(tag_ids)[:n_selected_tag])
user_tag_map = {}

with open('user_taggedbookmarks.dat') as dataset_file:
    dataset_file.readline()

    for line in dataset_file:
        line = line.split()
        tag_id = int(line[2])
        user_id = int(line[0])

        if tag_id in selected_tag:

            if user_id not in user_tag_map:
                user_tag_map[user_id] = [tag_id]
            else:
                user_tag_map[user_id].append(tag_id)

sorted_tag = sorted(list(selected_tag))
tag_map = {tag_id: idx for tag_id, idx in zip(sorted_tag, range(n_selected_tag))}
user_tag_map = {user_id: set([tag_map[tid] for tid in tag_id]) for user_id, tag_id in user_tag_map.iteritems()}

# for k, v in user_tag_map.iteritems():
#     print k, v
# print len(user_tag_map.keys())
# print tag_map

pairs = set()
for v in user_tag_map.itervalues():
        for p in permutations(v, 2):
            pairs.add(p)

# for p in pairs:
#     print p

# for p in pairs:
#     if p[0] == p[1]:
#         print p

adj_mat = np.zeros((n_selected_tag, n_selected_tag))
for p in pairs:
    adj_mat[p] += 1

print np.diag(adj_mat)

community = {i: [i] for i in xrange(adj_mat.shape[0])}
m = np.sum(adj_mat) / 2
k = np.sum(adj_mat, axis=1)
print community

delta_Q = np.zeros(adj_mat.shape)
for sort_idx, a in np.ndenumerate(adj_mat):
    if a > 0:
        delta_Q[sort_idx] = (1 / (2 * m)) - ((k[sort_idx[0]] * k[sort_idx[1]]) / (2 * m) ** 2)

a = k / (2*m)
current_Q = 0

counter = 0
while len(community) > 2:
    max_idx = np.unravel_index(np.argmax(delta_Q), delta_Q.shape)
    current_Q += delta_Q[max_idx]

    # gabung i ke j (update)
    c_i = max_idx[0]
    c_j = max_idx[1]

    for c_k in community.iterkeys():
        if not (c_k == c_j or c_k == c_i):
            connected_to_i = is_connected(adj_mat, community[c_i], community[c_k])
            connected_to_j = is_connected(adj_mat, community[c_j], community[c_k])

            if connected_to_i and connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] + delta_Q[c_j, c_k]
            elif connected_to_i:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] - (a[c_j] * a[c_k])
            elif connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_j, c_k] - (a[c_i] * a[c_k])

    delta_Q[:, c_j] = delta_Q[c_j, :].T
    delta_Q[c_i, :] = -np.inf
    delta_Q[:, c_i] = -np.inf

    a[c_j] += a[c_i]
    a[c_i] = 0

    community[c_j].extend(community[c_i])
    del community[c_i]

    print max_idx,
    print current_Q
    # print community