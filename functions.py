import numpy as np
from itertools import product


def is_connected(adjency_matrix, c1, c2):
    idx1, idx2 = [], []
    for p in product(c1, c2):
        idx1.append(p[0])
        idx2.append(p[1])

    return np.sum(adjency_matrix[idx1, idx2]) > 0


def euclidian_distance(a, b):
    return np.sqrt(np.sum((np.asarray(a) - np.asarray(b)) ** 2))


def unravel(delta_q, community):
    current_q = -1
    besta = 0
    bestb = 1
    for a in range(0, len(delta_q[0])):
        for b in range(a, len(delta_q[0])):
            if a != b and community.has_key(a) and community.has_key(b) and current_q < delta_q[a, b]:
                besta = a
                bestb = b
                current_q = delta_q[a, b]
    return besta, bestb


def unravel2(delta_q):
    curq = 0
    bestindex = None
    for key in delta_q.keys():
        if delta_q[key] > curq:
            bestindex = key
            curq = delta_q[key]

    return bestindex
